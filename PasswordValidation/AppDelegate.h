//
//  AppDelegate.h
//  PasswordValidation
//
//  Created by Joan Barrull Ribalta on 14/03/16.
//  Copyright © 2016 com.giria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

