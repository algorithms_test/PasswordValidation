//
//  AppDelegate.m
//  PasswordValidation
//
//  Created by Joan Barrull Ribalta on 14/03/16.
//  Copyright © 2016 com.giria. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    NSString * test = @"1Mawbc";
    Boolean valid = [self isValid:test];
    NSLog(@" isValid: %d", valid);
    int result = [self solution: test];
    NSLog(@" Result: %d", result);
    return YES;
}

/* Return true if the string constains at lest one uppercase character and no numbers */

- (int) solution: (NSString *)A  {
    int result = 0;
    for(int endIndex=0; endIndex < A.length ; endIndex++) {
        NSString * subString =  [A substringToIndex:endIndex +1];
        NSLog(@"Substring: %@", subString);
        if (  [self isValid:subString] ) result = subString.length;
    }
 
    
    return result;
}



- (Boolean) isValid:(NSString*) substring {
    
    int numberOfUpperCase = 0;
    for (int i=0;i<substring.length; i++) {
        char c =  [substring characterAtIndex:i];
        BOOL isUppercase = [[NSCharacterSet uppercaseLetterCharacterSet] characterIsMember:c];
        if (isUppercase) { numberOfUpperCase++;};
        //if(c>='0' && c<='9') { return false;}
        if ( isdigit(c) ) {return false;};
    }
    if (numberOfUpperCase < 1) return false;
    return true;
}





- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
